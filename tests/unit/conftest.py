from typing import List
from unittest import mock

import pytest


@pytest.fixture(scope="session", name="hub")
def unit_hub(hub):
    hub.pop.sub.add(dyne_name="idem")
    yield hub


@pytest.fixture(scope="session", autouse=True)
def acct_subs() -> List[str]:
    return ["vault"]


@pytest.fixture(scope="session", autouse=True)
def acct_profile() -> str:
    # The name of the profile that will be used for tests
    # Do NOT use production credentials for tests!
    # A profile with this name needs to be defined in the $ACCT_FILE in order to run these tests
    return "test_development_idem_vault"


async def _setup_hub(hub):
    # Set up the hub before each function here
    ...


async def _teardown_hub(hub):
    # Clean up the hub after each function here
    ...


@pytest.fixture(scope="function", autouse=True)
async def function_hub_wrapper(hub):
    await _setup_hub(hub)
    yield
    await _teardown_hub(hub)


@pytest.fixture(scope="function")
def mock_hub(hub):
    m_hub = hub.pop.testing.mock_hub()
    m_hub.OPT = mock.MagicMock()
    m_hub.SUBPARSER = mock.MagicMock()
    yield m_hub
