import copy
import uuid
from typing import Dict

import pytest


@pytest.mark.asyncio
async def test_secret(hub, ctx, version):
    if version == "v1":
        return
    secret_name = "idem-test-kv-v2-secret-" + str(uuid.uuid4())
    path = "secret/idem-test-kv-v2-secret-1"
    data = {"my-secret": "my-secret-value"}
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.vault.secrets.kv_v2.secret.present(
        ctx=test_ctx, name=secret_name, path=path, data=data
    )
    try:
        assert ret["result"], ret["comment"]
        assert (
            f"Would create vault.secrets.kv_v2.secret '{secret_name}'."
            in ret["comment"]
        )
        assert not ret["old_state"] and ret["new_state"]
        verify_parameters(
            expect_name=secret_name,
            expect_path=path,
            expect_data=data,
            resource=ret["new_state"],
        )

        ret = await hub.states.vault.secrets.kv_v2.secret.present(
            ctx=ctx, name=secret_name, path=path, data=data
        )
        assert ret["result"], ret["comment"]
        assert f"Created vault.secrets.kv_v2.secret '{secret_name}'." in ret["comment"]
        assert not ret["old_state"] and ret["new_state"]
        verify_parameters(
            expect_name=secret_name,
            expect_path=path,
            expect_data=data,
            resource=ret["new_state"],
        )

        new_data = {"my-secret": "my-new-secret-value"}
        # Update secret with --test
        ret = await hub.states.vault.secrets.kv_v2.secret.present(
            ctx=test_ctx, name=secret_name, path=path, data=new_data
        )
        assert ret["result"], ret["comment"]
        assert (
            f"Would update vault.secrets.kv_v2.secret '{secret_name}'."
            in ret["comment"]
        )
        assert ret["old_state"] and ret["new_state"]
        verify_parameters(
            expect_name=secret_name,
            expect_path=path,
            expect_data=data,
            resource=ret["old_state"],
        )
        verify_parameters(
            expect_name=secret_name,
            expect_path=path,
            expect_data=new_data,
            resource=ret["new_state"],
        )

        # Update secret in real
        ret = await hub.states.vault.secrets.kv_v2.secret.present(
            ctx=ctx, name=secret_name, path=path, data=new_data
        )
        assert ret["result"], ret["comment"]
        assert f"Updated vault.secrets.kv_v2.secret '{secret_name}'." in ret["comment"]
        assert ret["old_state"] and ret["new_state"]
        verify_parameters(
            expect_name=secret_name,
            expect_path=path,
            expect_data=data,
            resource=ret["old_state"],
        )
        verify_parameters(
            expect_name=secret_name,
            expect_path=path,
            expect_data=new_data,
            resource=ret["new_state"],
        )

        # Change secret in real with disable read
        ret = await hub.states.vault.secrets.kv_v2.secret.present(
            ctx=ctx, name=secret_name, path=path, data=new_data, disable_read=True
        )
        assert ret["result"], ret["comment"]
        # If read is disabled, there is no way for Idem to know if the secret has existed or not. So, Idem always assume
        # that it is a creation operation.
        assert f"Created vault.secrets.kv_v2.secret '{secret_name}'." in ret["comment"]
        assert not ret["old_state"] and ret["new_state"]
        verify_parameters(
            expect_name=secret_name,
            expect_path=path,
            expect_data=new_data,
            resource=ret["new_state"],
        )

        # Test search()
        ret = await hub.states.vault.secrets.kv_v2.secret.search(
            ctx=ctx, name=secret_name, path=path, version=2
        )
        assert ret["result"], ret["comment"]
        assert ret["old_state"] and ret["new_state"]
        verify_parameters(
            expect_name=secret_name,
            expect_path=path,
            expect_data=new_data,
            resource=ret["old_state"],
        )
        verify_parameters(
            expect_name=secret_name,
            expect_path=path,
            expect_data=new_data,
            resource=ret["new_state"],
        )

        # Delete secret with --test
        ret = await hub.states.vault.secrets.kv_v2.secret.absent(
            ctx=test_ctx, name=secret_name, path=path
        )
        assert ret["result"], ret["comment"]
        assert (
            f"Would delete vault.secrets.kv_v2.secret '{secret_name}'."
            in ret["comment"]
        )
        assert ret["old_state"] and not ret["new_state"]
        verify_parameters(
            expect_name=secret_name,
            expect_path=path,
            expect_data=None,
            resource=ret["old_state"],
        )

    finally:
        # Delete secret in real
        ret = await hub.states.vault.secrets.kv_v2.secret.absent(
            ctx=ctx, name=secret_name, path=path
        )
        assert ret["result"], ret["comment"]
        assert f"Deleted vault.secrets.kv_v2.secret '{secret_name}'." in ret["comment"]
        assert ret["old_state"] and not ret["new_state"]
        verify_parameters(
            expect_name=secret_name,
            expect_path=path,
            expect_data=None,
            resource=ret["old_state"],
        )

    # Verify the secret has been destroyed in Vault
    read_ret = await hub.exec.hvac.client.secrets.kv.v2.read_secret_version(
        ctx=ctx, path=path, version=3
    )
    assert read_ret["result"] is False
    assert "InvalidPath" in str(read_ret["comment"])

    # Delete secret a second time should result to no-op
    ret = await hub.states.vault.secrets.kv_v2.secret.absent(
        ctx=ctx, name=secret_name, path=path
    )
    assert ret["result"], ret["comment"]
    assert (
        f"vault.secrets.kv_v2.secret '{secret_name}' is already absent."
        in ret["comment"]
    )
    assert not ret["old_state"] and not ret["new_state"]


@pytest.mark.asyncio
async def test_delete_secret_all_versions(hub, ctx, version):
    if version == "v1":
        return
    secret_name = "idem-test-kv-v2-secret-" + str(uuid.uuid4())
    path = "secret/idem-test-kv-v2-secret-2"
    data = {"my-secret": "my-secret-value"}
    ret = await hub.states.vault.secrets.kv_v2.secret.present(
        ctx=ctx, name=secret_name, path=path, data=data
    )
    try:
        assert ret["result"], ret["comment"]
        assert f"Created vault.secrets.kv_v2.secret '{secret_name}'." in ret["comment"]

        new_data = {"my-secret": "my-new-secret-value"}
        ret = await hub.states.vault.secrets.kv_v2.secret.present(
            ctx=ctx, name=secret_name, path=path, data=new_data
        )
        assert ret["result"], ret["comment"]
        assert f"Updated vault.secrets.kv_v2.secret '{secret_name}'." in ret["comment"]

        test_ctx = copy.deepcopy(ctx)
        test_ctx["test"] = True
        # Delete secret with all versions with test
        ret = await hub.states.vault.secrets.kv_v2.secret.absent(
            ctx=test_ctx, name=secret_name, path=path, delete_all_versions=True
        )
        assert ret["result"], ret["comment"]
        assert (
            f"Would delete vault.secrets.kv_v2.secret '{secret_name}' all versions."
            in ret["comment"]
        )
        assert ret["old_state"] and not ret["new_state"]

    finally:
        # Delete secret with all versions
        ret = await hub.states.vault.secrets.kv_v2.secret.absent(
            ctx=ctx, name=secret_name, path=path, delete_all_versions=True
        )
        assert ret["result"], ret["comment"]
        assert (
            f"Deleted vault.secrets.kv_v2.secret '{secret_name}' all versions."
            in ret["comment"]
        )
        assert ret["old_state"] and not ret["new_state"]

    # Verify the secret has been destroyed in Vault
    read_ret = await hub.exec.hvac.client.secrets.kv.v2.read_secret_version(
        ctx=ctx, path=path
    )
    assert read_ret["result"] is False
    assert "InvalidPath" in str(read_ret["comment"])


def verify_parameters(
    expect_name: str, expect_path: str, expect_data: Dict or None, resource: Dict
):
    assert expect_name == resource.get("name")
    assert expect_path == resource.get("path")
    assert expect_data == resource.get("data")
